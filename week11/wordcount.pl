use strict;
use warnings;

my $file_name = $ARGV[0];
open FILE , '<' , $file_name or die "Can not open $file_name!\n";

my @lines = <FILE>;
close FILE;

my %word_freq = ();
foreach my $i (@lines)
{
   chomp $i;
   foreach my $word(split " ",$i)
   {    
       if(!exists $word_freq{$word}) {
            $word_freq{$word} = 1;
       }
       else {
            $word_freq{$word}++;
       }
   }
}
foreach my $key (keys %word_freq)
{
   print "$key $word_freq{$key}\n";
}

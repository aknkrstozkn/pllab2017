use strict;
use warnings;

my $x = "I'm a string";
print "\$x is a scalar variable. \$x = $x\n";

$x = 45;
print "\$x is a scalar variable. \$x = $x\n";

my @arr = ("Ali","Mehmet","Deniz"); 

$x = @arr;
print "\$x is a scalar variable. \$x = $x\n";

($x) = @arr;
print "\$x is a scalar variable. \$x = $x\n";
print "Size of \@arr is equal ".scalar @arr."\n";

my %grades = ("Ali" => 60, "Mehmet" => 45, "Deniz" => 89);
print $arr[1] . "'s grade is ". $grades{$arr[1]} ."\n";

my $dna = "ATGCCCATTGAC";
my $pattern1 = "GCC";
my $pattern2 = "TTT";

if($dna =~ /$pattern1/)
{
 print "$dna contains $pattern1 \n";
}else
{
 print "$dna does not contain $pattern1 \n";
}
if($dna =~ /$pattern2/)
{
 print "$dna contains $pattern2 \n";
}else
{
 print "$dna does not contain $pattern2 \n";
}
print "Single Substitution \n";
print "Old DNA is $dna \n";
$dna =~ s/GCC/TTA/;
print "New DNA is $dna \n";
print "Globa substitution \n";
print "Old DNA is $dna \n";
$dna =~ s/T/G/g;
print "New DNA is $dna \n";
$dna = "ATG CATTT   CGCATAG";
print "Old DNA is $dna \n";
$dna =~ s/\s//g;
print "New DNA is $dna \n";
$dna = "AtCGCATTCtAG";
print "Old DNA is $dna \n";
$dna =~ s/T/T/gi;

